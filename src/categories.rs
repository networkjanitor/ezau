use evtclib::{Encounter, GameMode, Log};

pub trait Categorizable {
    fn category(&self) -> &'static str;
}

impl Categorizable for Log {
    fn category(&self) -> &'static str {
        if self.game_mode() == Some(GameMode::WvW) {
            return "World versus World";
        }

        if let Some(encounter) = self.encounter() {
            match encounter {
                Encounter::ValeGuardian | Encounter::Gorseval | Encounter::Sabetha => {
                    "Wing 1 (Spirit Vale)"
                }
                Encounter::Slothasor | Encounter::BanditTrio | Encounter::Matthias => {
                    "Wing 2 (Salvation Pass)"
                }
                Encounter::KeepConstruct | Encounter::TwistedCastle | Encounter::Xera => {
                    "Wing 3 (Stronghold of the Faithful)"
                }
                Encounter::Cairn
                | Encounter::MursaatOverseer
                | Encounter::Samarog
                | Encounter::Deimos => "Wing 4 (Bastion of the Penitent)",
                Encounter::SoullessHorror
                | Encounter::RiverOfSouls
                | Encounter::BrokenKing
                | Encounter::EaterOfSouls
                | Encounter::StatueOfDarkness
                | Encounter::VoiceInTheVoid => "Wing 5 (Hall of Chains)",
                Encounter::ConjuredAmalgamate | Encounter::TwinLargos | Encounter::Qadim => {
                    "Wing 6 (Mythwright Gambit)"
                }
                Encounter::CardinalAdina
                | Encounter::CardinalSabir
                | Encounter::QadimThePeerless => "Wing 7 (Key of Ahdashim)",

                Encounter::Ai => "100 CM (Sunqua Peak)",
                Encounter::Skorvald | Encounter::Artsariiv | Encounter::Arkk => {
                    "99 CM (Shattered Observatory)"
                }
                Encounter::MAMA | Encounter::Siax | Encounter::Ensolyss => "98 CM (Nightmare)",

                Encounter::IcebroodConstruct
                | Encounter::SuperKodanBrothers
                | Encounter::FraenirOfJormag
                | Encounter::Boneskinner
                | Encounter::WhisperOfJormag => "Strike Mission",

                Encounter::StandardKittyGolem
                | Encounter::MediumKittyGolem
                | Encounter::LargeKittyGolem => "Special Forces Training Area",

                _ => "Unknown",
            }
        } else {
            "Unknown"
        }
    }
}
